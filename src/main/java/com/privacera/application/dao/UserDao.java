package com.privacera.application.dao;

import com.privacera.application.model.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserDao extends JpaRepository<User,Long> {
}
